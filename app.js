const express = require('express');
require('dotenv').config();
const PORT = process.env.PORT || 3000;
const app = express();

app.route('/user').get((req, res) => {
    console.log('url:', req.url);
    res.send('Hello from User Manager!');
})
app.route('/user').post((req, res) => {
    console.log('url:', req.url);
    res.send('Hello from User POST!');
})
app.route('/user').put((req, res) => {
    console.log('url:', req.url);
    res.send('Hello from User PUT!');
})
app.route('/user').delete((req, res) => {
    console.log('url:', req.url);
    res.send('Hello from User Delete!');
})


app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
})